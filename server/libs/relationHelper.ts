import * as queue from "queue";
import mark from "../models/Marks";
import country from "../models/Countries";
import color from "../models/Collors";
import galleries from "../models/Galleries";

/**
 * Интерфейс описывающий содержание класса
 */
interface IClass {
    name: string,
    table: string,
    slug: string
}

export module RelationHelper {

    /**
     * Подгружает связи к продукции, такие как производитель, цвета, марка
     * @param models
     * @param category
     * @returns {Promise|Promise<T>}
     */
    export function loadRelations(models, category) {

        let marksList = [],
            collorsList = [],
            countryList = [],
            galleryList = [];

        return new Promise((succeed, fail) => {
            let q = queue();

            // country
            q.push(function (next) {
                country.find(function (error, data) {

                    if (!error) {
                        for (let i = 0; i < data.length; i++) {
                            countryList[data[i].id] = data[i].name;
                        }
                    }
                    next();
                })

            });

            // color
            q.push(function (next) {

                color.find(function (error, data) {
                    if (!error) {
                        for (let i = 0; i < data.length; i++) {
                            collorsList[data[i].id] = data[i].name;

                        }
                    }
                    next();
                })

            });

            // mark
            q.push(function (next) {

                mark.find(function (error, data) {
                    if (!error) {
                        for (let i = 0; i < data.length; i++) {
                            marksList[data[i].id] = data[i].name;
                        }
                    }
                    next();
                })

            });

            // gallery
            q.push(function (next) {

                galleries.find({category: category}, function (error, data) {

                    if (!error && data.length > 0) {

                        for (let i = 0; i < data.length; i++) {
                            //let id = data[i].item;
                            //if (!galleryList[id]) galleryList[id] = [];

                            //galleryList[id].push(data[i]);
                        }
                    }
                    next();
                })

            });

            q.start(function (err) {

                let id = "";
                console.log('relationsHelers', models.length);
                for (let i = 0; i < models.length; i++) {
                    id = models[i]["mark"];
                    console.log('mark', id );
                    if (id) {
                        if (marksList[id]) models[i]["mark"] = marksList[id];
                        else models[i]["mark"] = "";
                    }

                    id = models[i]["color"];
                    if (id) {
                        if (collorsList[id]) models[i]["color"] = collorsList[id];
                        else models[i]["color"] = "";
                    }

                    id = models[i]["manufacturer"];
                    if (id) {
                        if (countryList[id]) models[i]["manufacturer"] = countryList[id];
                        else models[i]["manufacturer"] = "";
                    }

                    if (category) {

                        models[i].category = category;
                    }

                    if (galleryList[models[i].id] && galleryList[models[i].id].length > 0) {
                        models[i].gallery = galleryList[models[i].id];
                    }
                    //console.log('models category', models[i], models[i]['category']);

                }

                if (err) {
                    if (fail && typeof(fail) == "function") fail(err);
                }
                else if (succeed && typeof(succeed) == "function") succeed(models);

            });
        });
    }

    /**
     * Опредеяем класс продукции по категории
     * @returns {Array<IClass>}
     */
    export function getProductClasses(): Array<IClass> {

        let list: Array<IClass> = [];
        list.push({name: "Холодильники", table: "Productsfridges", slug: "fridges"});
        list.push({name: "Стиральные машинки", table: "Productswashings", slug: "washings"});
        list.push({name: "Панели", table: "Productshobs", slug: "hobs"});
        list.push({name: "Кондиционеры", table: "Productsconditioners", slug: "conditioners"});
        list.push({name: "Духовые шкафы", table: "Productsovens", slug: "ovens"});
        return list;
    }

    /**
     * Выводит таблицу по названию категории
     * @param category
     * @returns {any}
     */
    export function getProductClass(category: string): string {

        let list: Array<IClass> = getProductClasses();

        if (category) {

            return list.find((element) => {
                if (element.slug == category)return element;
            });
        }

        return "";
    }


}

