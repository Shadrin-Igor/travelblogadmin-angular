angular.module('starter')

    .controller('LoginCtrl', function($scope, AuthService, $state) {
        $scope.user = {
            name: '',
            password: ''
        };

        $scope.login = function() {
            AuthService.login($scope.user).then(function(msg) {
                $state.go('inside');
            }, function(errMsg) {
                alert('Login failed!'+errMsg);
            });
        };
    })

    .controller('RegisterCtrl', function($scope, AuthService, $state) {
        $scope.user = {
            name: '',
            password: ''
        };

        $scope.signup = function() {
            AuthService.register($scope.user).then(function(msg) {
                $state.go('login');
                alert('Register success!'+msg);
            }, function(errMsg) {
                alert('Register failed!'+errMsg);
            });
        };
    })

    .controller('InsideCtrl', function($scope, $http, $window, AuthService, API_ENDPOINT, $state) {
        $scope.destroySession = function() {
            AuthService.logout();
        };

        $scope.getInfo = function() {

            $http.get(API_ENDPOINT.url + '/post').then(function(result) {
                $scope.memberinfo = result.data.msg;
            });
        };

        $scope.logout = function() {
            console.log('logout');
            AuthService.logout();
            $state.go('login');
        };
    })

    .controller('AppCtrl', function($scope, $state, AuthService, AUTH_EVENTS) {
        $scope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
            AuthService.logout();
            $state.go('login');
            alert('Session Lost!'+'Sorry, You have to login again.');
        });
    });