angular.module('starter', ['ui.router'])

    .config(function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('inside', {
                url: '/inside',
                templateUrl: 'templates/inside.html',
                controller: 'InsideCtrl'
            })
            .state('outside', {
                url: '/outside',
                abstract: true,
                templateUrl: 'templates/outside.html'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'templates/register.html',
                controller: 'RegisterCtrl'
            })
            ;

        $urlRouterProvider.otherwise('/login');
    })

    .run(function ($rootScope, $state, $log, AuthService, AUTH_EVENTS) {
        $rootScope.$on('$stateChangeStart', function (event,next, nextParams, fromState) {
            if (!AuthService.isAuthenticated()) {
                console.log(next.name);
                if (next.name !== 'login' && next.name !== 'register') {
                    event.preventDefault();
                    $state.go('login');
                }
            }
        });

        $rootScope.$on('$stateChangeError',function (event, toState, toParams, fromState, fromParams, error) {
                if (!angular.isString(error)) {
                    error = JSON.stringify(error);
                }
                $log.error('$stateChangeError: ' + error);
            }
        );

    });