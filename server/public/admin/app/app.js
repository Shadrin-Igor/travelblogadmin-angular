angular.module('mainApp', ['ui.router', 'dataGrid', 'pagination', 'angularFileUpload', 'ngAnimate', 'toastr'])

    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: 'app/views/index.html',
                controller: 'IndexController',
            })
            .state('login', {
                url: '/login',
                templateUrl: 'app/views/login.html',
                controller: 'LoginController',
            })
            .state('products', {
                url: '/products/:cat',
                templateUrl: 'app/views/products.html',
                controller: 'ProductsController',
                controllerAs: 'productsCtrl'
            })
            .state('product', {
                url: '/product/:cat/:id',
                templateUrl: 'app/views/product-edit.html',
                controller: 'ProductController',
                controllerAs: 'productCtrl'
            });

        $urlRouterProvider.otherwise('/login');
    })

    .run(function ($rootScope, $state, $log, AuthService, AUTH_EVENTS) {
        $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
            //console.log("stateChangeStart", next, nextParams, fromState);
            if (!AuthService.isAuthenticated()) {
                if (next.name !== 'login' && next.name !== 'register') {
                    event.preventDefault();
                    $state.go('login');
                }
            }
        });

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
                if (!angular.isString(error)) {
                    error = JSON.stringify(error);
                }
                $log.error('$stateChangeError: ' + error);
            }
        );

    });