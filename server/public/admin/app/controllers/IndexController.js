(function() {

    'use strict';

    angular
        .module('mainApp')

        /**
         * IndexController
         * Description: Sets up a controller
         */
        .controller('IndexController', IndexController);
        function IndexController($scope, $rootScope, $http, $window, AuthService, API_ENDPOINT, $state) {
            $scope.destroySession = function() {
                AuthService.logout();
            };

            $scope.getInfo = function() {

                $http.get(API_ENDPOINT.url + '/post').then(function(result) {
                    $scope.memberinfo = result.data.msg;
                });
            };


        };

})();