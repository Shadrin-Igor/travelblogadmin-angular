var express = require('express'),
    router = express.Router(),
    moduleKey = 'upload',
    passport = require('passport'),
    fs = require('fs'),
    multiparty = require('multiparty'),
    im = require('imageMagick'),
    queue = require("queue");

var galleries = require("../../models/Galleries");

router.post('/' + moduleKey + '/:cat/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var category = req.params.cat;
    var itemId = req.params.id;

    console.log('post');
    // создаем форму
    var form = new multiparty.Form();
    //здесь будет храниться путь с загружаемому файлу, его тип и размер
    var uploadFile = {uploadPath: '', type: '', size: 0};
    //максимальный размер файла
    var maxSize = 2 * 1024 * 1024; //2MB
    //поддерживаемые типы(в данном случае это картинки формата jpeg,jpg и png)
    var supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png'];
    //массив с ошибками произошедшими в ходе загрузки файла
    var errors = [];

    //если произошла ошибка
    form.on('error', function (err) {
        if (fs.existsSync(uploadFile.path)) {
            //если загружаемый файл существует удаляем его
            fs.unlinkSync(uploadFile.path);
            console.log('error');
        }
    });

    form.on('close', function () {
        //если нет ошибок и все хорошо
        if (errors.length == 0) {

            var q = queue({concurrency: 1}),
                itemModel = {},
                itemCountImage = 0,
                uploadError = '',
                params = '',
                file = '',
                dir = '',
                fileName = '',
                dirName = '',
                fileDtr = '',
                smallFileDtr = '',
                bigFileDtr = '';

            if (!category || !itemId)uploadError = "Upload image error: have't category or id item";

            // Выставкиваем модель по ID и категории
            q.push(function (next) {

                if (!uploadError) {
                    switch (category) {
                        case 'fridges' :
                            var modelName = 'Productsfridges';
                            break;
                        case 'washings' :
                            var modelName = 'Productswashings';
                            break;
                        case 'hobs' :
                            var modelName = 'Productshobs';
                            break;
                        case 'conditioners' :
                            var modelName = 'Productsconditioners';
                            break;
                        case 'ovens' :
                            var modelName = 'Productsovens';
                            break;
                        default :
                            var modelName = 'Productsfridges';
                    }
                    var model = require('../../models/' + modelName);
                    model.findOne({id: itemId}, function (err, data) {
                        if (!err) {
                            if (data && data.id)itemModel = data;
                            else uploadError = 'incorrect ID or category - ' + modelName + ' - ' + itemId;
                        } else {
                            uploadError = err;
                        }
                        next();
                    });
                }
                else next();
            });

            // создаем категории
            q.push(function (next) {

                if (!uploadError) {
                    params = uploadFile.path.split("/");
                    file = params[params.length - 1];
                    params[params.length - 1] = '';
                    dir = '/upload/';//params.join("/");

                    try {
                        fs.mkdirSync('./public'+dir + category + '/' + itemId);
                    } catch (err) {
                        if (err) {

                            try {

                                fs.mkdirSync('./public'+dir + category);
                                fs.mkdirSync('./public'+dir + category + '/' + itemId);

                            }
                            catch (err) {
                                //console.log(err);
                                //uploadError = 'problem with create folder';
                            }
                        }

                    }

                    dir += category + '/' + itemId;
                    next();
                }
                else next();
            });

            // Закачиваем оригинальный файл
            q.push(function (next) {
                console.log("push 1");
                if (!uploadError) {
                    fs.rename(uploadFile.path, './public'+dir + '/' + file, function (err) {
                        console.log("push 2");
                        if (err) uploadError = err;
                        else {
                            fileName = file;
                            dirName = dir + '/';
                            fileDtr = './public'+dir + '/' + file;
                            smallFileDtr = './public'+dir + '/thumb_' + file;
                            bigFileDtr = './public'+dir + '/big_' + file;
                        }

                        next();
                    })
                }
                else next();

            });

            // Закачиваем большой файл 800x600
            q.push(function (next) {
                console.log("push 3");
                if (!uploadError) {
                    im.convert([fileDtr, '-resize', '800x600', fileDtr],
                        function (err, stdout) {
                            if (err) uploadError = err;
                            next();
                        })
                }
                else next();
            });

            // Закачиваем маленький файл 85x85
            q.push(function (next) {
                console.log("push 4");
                if (!uploadError) {
                    im.convert([fileDtr, '-resize', '85x85', smallFileDtr],
                        function (err, stdout) {
                            if (err) uploadError = err;
                            next();
                        })
                }
                else next();
            });

            // Закачиваем маленький файл 350x380
            q.push(function (next) {
                if (!uploadError) {
                    im.convert([fileDtr, '-resize', '350x380', bigFileDtr],
                        function (err, stdout) {
                            if (err) uploadError = err;
                            next();
                        })
                }
                else next();
            });

            // Сохраняем в галлерею
            q.push(function (next) {
                if (!uploadError) {
                    var image = new galleries();
                    image.dir = dirName.replace("../", "").replace("server/public/", "");

                    image.image = fileName;
                    image.category = category;
                    image.item = itemId;
                    image.save()
                        .then(function (data) {

                            next();

                        }, function (err) {
                            uploadError = err;
                            next();
                        })
                }
                else next();

            });

            // Проверяем солько уже фотографий у это продукции
            q.push(function (next) {
                console.log("push 8");
                if (!uploadError) {

                    galleries.count({category: category, item: itemId}, function (error, count) {
                        if (!error) {
                            itemCountImage = count;
                        }
                        next();
                    })

                } else next();
            });

            // Если у продукции нет картинки сохраняем первую картинки в поле image
            q.push(function (next) {
                console.log("push 9", itemCountImage);
                // Сохраняемкартинку только если это первая картинки в галларее
                if (!uploadError && itemCountImage == 1) {
                    itemModel.image = fileName;
                    itemModel.image_dir = dirName.replace("../", "").replace("server/public/", "");
                    console.log('itemModel', itemModel);
                    itemModel.save()
                        .then(function (data) {
                            next();
                        }, function (error) {
                            uploadError = error;
                            next();
                        })

                } else next();
            });

            q.start(function (err) {
                console.log("push finish");
                if (!uploadError) {

                    //сообщаем что все хорошо
                    res.send({
                        status: 'ok',
                        text: 'Success',
                        file: {
                            image: fileName,
                            dir: dirName.replace("../", "").replace("server/public/", "")
                        }
                    });
                } else {
                    res.send({
                        status: 'fail',
                        error: uploadError
                    });
                }
            });

            /*fs.rename(uploadFile.path, dir + '/' + file, function (err) {
             if (err) throw err;

             fileName = file;
             dirName = dir + '/';
             fileDtr = dir + '/' + file;
             smallFileDtr = dir + '/thumb_' + file;
             bigFileDtr = dir + '/big_' + file;

             im.convert([fileDtr, '-resize', '800x600', fileDtr],
             function (err, stdout) {
             if (err) res.send({status: 'fail', error: err});

             im.convert([fileDtr, '-resize', '85x85', smallFileDtr],
             function (err, stdout) {
             if (err) res.send({status: 'fail', error: err});

             im.convert([fileDtr, '-resize', '350x380', bigFileDtr],
             function (err, stdout) {
             if (err) res.send({status: 'fail', error: err});

             var image = new galleries();
             image.dir = dirName.replace("../", "").replace("server/public/", "");

             image.image = fileName;
             image.category = category;
             image.item = itemId;
             image.save()
             .then(function (data) {

             //сообщаем что все хорошо
             res.send({
             status: 'ok',
             text: 'Success',
             file: {
             image: fileName,
             dir: dirName.replace("../", "").replace("server/public/", "")
             }
             });

             }, function (err) {
             throw err;
             })

             });
             });
             });
             })*/

        }
        else {
            if (fs.existsSync(uploadFile.path)) {
                //если загружаемый файл существует удаляем его
                fs.unlinkSync(uploadFile.path);
            }
            //сообщаем что все плохо и какие произошли ошибки
            res.send({status: 'bad', errors: errors});
        }
    });

    // при поступление файла
    form.on('part', function (part) {

        //читаем его размер в байтах
        uploadFile.size = part.byteCount;
        //читаем его тип
        uploadFile.type = part.headers['content-type'];
        //путь для сохранения файла
        uploadFile.path = '../server/public/upload/' + part.filename;

        //проверяем размер файла, он не должен быть больше максимального размера
        if (uploadFile.size > maxSize) {
            errors.push('File size is ' + uploadFile.size + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
        }

        //проверяем является ли тип поддерживаемым
        if (supportMimeTypes.indexOf(uploadFile.type) == -1) {
            errors.push('Unsupported mimetype ' + uploadFile.type);
        }

        //если нет ошибок то создаем поток для записи файла
        if (errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.path);
            part.pipe(out);
        }
        else {
            //пропускаем
            //вообще здесь нужно как-то остановить загрузку и перейти к onclose
            part.resume();
        }
    });

    // парсим форму
    form.parse(req);

});

module.exports = router;