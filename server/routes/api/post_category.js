var express = require('express');
var router = express.Router();
var moduleKey = 'post_category';
var PostsCategory = require('../../models/PostsCategory');

router.get('/'+moduleKey, function(req, res, next) {

    PostsCategory.find(function(err, data) {
        res.json(data);
    });
});

router.post('/'+moduleKey, function(req, res, next) {
    res.json('POST ABOUT');
});

module.exports = router;