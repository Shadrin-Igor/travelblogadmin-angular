var express = require('express');
var router = express.Router();
var passport = require('passport');
var Marks = require('../../models/Marks');
var moduleKey = 'marks';

router.get('/' + moduleKey , passport.authenticate('jwt', {session: false}), function (req, res, next) {

    Marks.find(function (err, data) {
        if (err)res.send({error: err});
            else res.send({data: data});
    })
});

module.exports = router;