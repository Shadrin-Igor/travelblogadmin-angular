var express = require('express');
var router = express.Router();
var passport = require('passport');
var Galleries = require('../../models/Galleries');
var moduleKey = 'gallery';

router.get('/' + moduleKey + '/:cat/:id' , passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var cat = req.params.cat;
    var id = req.params.id;
    if (cat && id) {
        Galleries.find({category:cat, item:id} ,function (err, data) {
            if (err)res.send({error: err});
            else res.send({data: data});
        })
    }
     else res.send({error: 'get gallery error: have\'t parameters'});


});

module.exports = router;