var express = require('express');
var router = express.Router();
var moduleKey = 'post';
var Posts = require('../../models/Posts');
var passport = require('passport');

router.get('/'+moduleKey, passport.authenticate('jwt', { session: false}), function(req, res, next) {

    Posts.find(function(err, data) {
        res.json(data);
    });
});

module.exports = router;