"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let express = require('express'), router = express.Router(), queue = require('queue');
const Marks_1 = require("../models/Marks");
const Productsfridges_1 = require("../models/Productsfridges");
const relationHelper_1 = require("../libs/relationHelper");
const Productsconditioners_1 = require("../models/Productsconditioners");
const Productswashings_1 = require("../models/Productswashings");
const Productshobs_1 = require("../models/Productshobs");
const Productsovens_1 = require("../models/Productsovens");
router.get('/', function (req, res, next) {
    let q = queue(), listProducts = [], listBrands = [];
    q.push((next) => {
        Productsfridges_1.default.find().limit(4)
            .then(function (list) {
            relationHelper_1.RelationHelper.loadRelations(list, 'fridges')
                .then(function (models) {
                listProducts = listProducts.concat(models);
                next();
            }, function (error) {
                console.log('error', error);
                next(error);
            });
        }, function (error) {
            console.log('ERROR index load items', error);
            next(error);
        });
    });
    q.push((next) => {
        Productsconditioners_1.default.find().limit(4)
            .then(function (list) {
            relationHelper_1.RelationHelper.loadRelations(list, 'conditioners')
                .then(function (models) {
                listProducts = listProducts.concat(models);
                next();
            }, function (error) {
                console.log('error', error);
                next(error);
            });
        }, function (error) {
            console.log('ERROR index load items', error);
            next(error);
        });
    });
    q.push((next) => {
        Productswashings_1.default.find().limit(5)
            .then(function (list) {
            relationHelper_1.RelationHelper.loadRelations(list, 'washings')
                .then(function (models) {
                listProducts = listProducts.concat(models);
                next();
            }, function (error) {
                console.log('error', error);
                next(error);
            });
        }, function (error) {
            console.log('ERROR index load items', error);
            next(error);
        });
    });
    q.push((next) => {
        Productshobs_1.default.find().limit(4)
            .then(function (list) {
            relationHelper_1.RelationHelper.loadRelations(list, 'hobs')
                .then(function (models) {
                listProducts = listProducts.concat(models);
                next();
            }, function (error) {
                console.log('error', error);
                next(error);
            });
        }, function (error) {
            console.log('ERROR index load items', error);
            next(error);
        });
    });
    q.push(function (next) {
        Productsovens_1.default.find().limit(4)
            .then(function (list) {
            relationHelper_1.RelationHelper.loadRelations(list, 'ovens')
                .then(function (models) {
                listProducts = listProducts.concat(models);
                next();
            }, function (error) {
                console.log('error', error);
                next();
            });
        }, function (error) {
            console.log('ERROR index load items', error);
            next(error);
        });
    });
    q.push(function (next) {
        Marks_1.default.find({}).sort('name').exec(function (err, data) {
            if (err) {
                next(err);
            }
            else {
                listBrands = data;
                next();
            }
        });
    });
    q.start(function (error) {
        if (error)
            console.log('ERROR index load items', error);
        else {
            res.render('index', { listProducts: listProducts, listBrands: listBrands });
        }
    });
});
module.exports = router;
//# sourceMappingURL=index.js.map