"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const relationHelper_1 = require("../libs/relationHelper");
let express = require("express"), router = express.Router(), queue = require("queue"), errorQueue = "";
const Marks_1 = require("../models/Marks");
router.get("/(:brand).html", (req, res, next) => {
    if (req.params.brand) {
        let modelClasses = relationHelper_1.RelationHelper.getProductClasses(), q = queue({ concurrency: 1 }), allItems = {}, errorQueue = "", listBrands = [], markModel = {}, model;
        q.push((next) => {
            Marks_1.default.findOne({ slug: req.params.brand }, (error, mark) => {
                if (!error && mark) {
                    markModel = mark;
                }
                next();
            });
        });
        if (markModel) {
            for (let i = 0; i < modelClasses.length; i++) {
                q.push(function (next) {
                    model = require("../models/" + modelClasses[i].table);
                    model.default.find({ mark: markModel.id })
                        .then(function (list, err) {
                        if (!err) {
                            if (list && list.length > 0) {
                                relationHelper_1.RelationHelper.loadRelations(list, modelClasses[i])
                                    .then(function (result) {
                                    allItems[modelClasses[i].slug] = result;
                                    next();
                                }, function (error) {
                                    console.log("error", error, modelClasses[i].table);
                                    errorQueue = error;
                                    next();
                                });
                            }
                            else {
                                next();
                            }
                        }
                        else {
                            errorQueue = err;
                            console.log("error Queue", modelClasses[i].table, err);
                            next();
                        }
                    });
                });
            }
        }
        q.push((next) => {
            Marks_1.default.find({}).sort("name").exec((err, data) => {
                if (err) {
                    next(err);
                }
                else {
                    listBrands = data;
                    next();
                }
            });
        });
        q.start((error) => {
            if (!errorQueue) {
                res.render("brand-list", { allItems: allItems, listBrands: listBrands, RelationHelper: relationHelper_1.RelationHelper });
            }
            else {
                console.log("errorQueue", errorQueue);
                res.redirect("/404.html");
            }
        });
    }
    else
        res.redirect("/404.html");
});
module.exports = router;
//# sourceMappingURL=brand.js.map