var express = require("express");
var router = express.Router();
var passport = require("passport");
var queue = require("queue");
var moment = require("moment");
var jwt = require('jwt-simple');

var postApi = require("./api/post.js");
var productsApi = require("./api/products.js");
var productApi = require("./api/product.js");
var marks = require("./api/marks.js");
var countries = require("./api/countries.js");
var collors = require("./api/collors");
var postCategoryApi = require("./api/post_category.js");
var admin = require("./api/admin.js");
var upload = require("./api/upload.js");
var importCSV = require("./api/import.js");
var gallery = require("./api/gallery.js");
var exportApi = require('./api/export.js');

var adminModel = require( "../models/Admins" );
var AccessToken = require("../models/AccessToken");

router.get('/', function(req, res, next) {
    res.json({'error': 'Invalid URL'});
});

router.get('/unauthorized', function(req, res, next) {
    res.json({'error': 'unauthorized'});
});

router.post('/auth', function(req, res, next){
    var email = req.body.email || '';
    var password = req.body.password || '';
    var error = '';
    if( email && password ){
        adminModel.findOne(function (err, adminDB) {

            if( err || adminDB.length == 0 ){
                if( adminDB.length == 0 )err = 'incorrect email or password';
                    else res.json({'auth' : 'fail', 'error' : err});
            }
            else {
                var q = queue({concurrency:1});
                var newAccessToken = '';
                if( adminDB.verifyPassword( password ) ) {

                    var ip  = req.connection.remoteAddress && req.connection.remoteAddress != '::1' ? req.connection.remoteAddress : '127.0.0.1';

                    q.push(function(cb) {

                        AccessToken.findOne({"adminId": adminDB.id})
                            .then( function (data) {

                                if (!err && data) {
                                    if( ip == data.ip ){
                                        AccessToken.update({_id:data._id}, {'date':moment().format()}, function(err, result){

                                            if( result && result.ok )newAccessToken = data;
                                                      else error = "auth db error";

                                            cb();
                                        });
                                    }
                                        else {
                                        // Удаляем еслит срхраненный IP не соответствует IP пользователя
                                        AccessToken.remove({_id:data._id}).then(function(err){
                                            cb();
                                        })
                                    }
                                }
                                else {
                                    if( err )error = err;
                                    cb();
                                }
                            }, function (err) {
                                error = err;
                                cb();
                            });

                    });

                    q.push(function(cb) {
                        if( !error && !newAccessToken ){
                            newAccessToken = new AccessToken({
                                'adminId': parseInt(adminDB.id),
                                'ip': ip,
                                'token': jwt.encode({id:adminDB.id},"devdacticIsAwesome")
                            });

                            newAccessToken.save()
                                .then(function (newToken) {
                                    newAccessToken = newToken;
                                    cb();
                                }, function (err) {
                                    console.log('error', err.message);
                                    error = err.message;
                                    cb();
                                });
                        }
                            else cb();
                    });
                }
                    else error = 'incorrect email or password';

                q.start(function(err) {
                    if(!error && !newAccessToken.token)error = "auth token error";
                    if( !error && newAccessToken )res.json({'success' : 'true', 'token':newAccessToken.token,'user':{'id':adminDB.id,'email':adminDB.email,'role':adminDB.role}});
                        else {
                        console.log( newAccessToken );
                        res.json({'auth' : 'fail', 'error': error});
                    }
                });
    /*            if( adminDB.verifyPassword( password ) ){
                    AccessToken.find( {"userId":adminDB.id}, function( error, data ){
                        if( data ){
                            data.update
                        }
                            else {
                            var newAccessToken = new AccessToken( {'userId':adminDB.id } );
                            newAccessToken.save()
                                .then(function (data) {
                                    res.json({'auth' : 'success', 'user':{'id':adminDB.id,'email':adminDB.email,'role':adminDB.role,'token':data.token}});
                                });
                        }

                    });*/
            }
        })

    } else error = 'not have required field';

     if(error)res.json({'auth' : 'fail', 'error' : error});

});

module.exports = []
    .concat(router)
    .concat(postApi)
    .concat(postCategoryApi)
    .concat(productsApi)
    .concat(productApi)
    .concat(upload)
    .concat(importCSV)
    .concat(marks)
    .concat(countries)
    .concat(collors)
    .concat(gallery)
    .concat(exportApi)
    .concat(admin);
