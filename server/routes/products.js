"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let express = require('express'), router = express.Router(), queue = require('queue');
const relationHelper_1 = require("../libs/relationHelper");
const Marks_1 = require("../models/Marks");
router.get('/:category.html', function (req, res, next) {
    if (req.params.category) {
        let q = queue(), listProducts = [], listBrands = [];
        q.push((next) => {
            let catalogModel = require('../models/Products' + req.params.category);
            catalogModel.default.find({})
                .then(function (list) {
                relationHelper_1.RelationHelper.loadRelations(list, req.params.category)
                    .then(function (models) {
                    listProducts = listProducts.concat(models);
                    next();
                }, function (error) {
                    console.log('error', error);
                    next(error);
                });
            }, function (error) {
                console.log('ERROR index load items', error);
                next(error);
            });
        });
        q.push(function (next) {
            Marks_1.default.find({}).sort('name').exec(function (err, data) {
                if (err) {
                    next(err);
                }
                else {
                    listBrands = data;
                    next();
                }
            });
        });
        q.start(function (error) {
            let categoryData = relationHelper_1.RelationHelper.getProductClass(req.params.category);
            if (error)
                console.log('ERROR index load items', error);
            else {
                res.render('index', { listProducts: listProducts, listBrands: listBrands, categoryName: categoryData.name });
            }
        });
    }
    else
        res.redirect("/404.html");
    ;
});
router.get('/:category/(:slug).html', function (req, res, next) {
    if (req.params.category && req.params.slug) {
        let modelClass = relationHelper_1.RelationHelper.getProductClass(req.params.category), listBrands = Array;
        if (modelClass) {
            let model = require('../models/' + modelClass.table), q = queue({ concurrency: 1 }), errorQueue = '', models = {}, otherItems = [];
            q.push(function (next) {
                model.default.findOne({ slug: req.params.slug }, function (err, list) {
                    if (!err) {
                        relationHelper_1.RelationHelper.loadRelations([list], req.params.category)
                            .then(function (result) {
                            models = Object.assign({}, result[0]._doc, { category: modelClass.slug });
                            next();
                        }, function (error) {
                            console.log('error', error);
                            errorQueue = error;
                            next();
                        });
                    }
                    else {
                        console.log('6');
                        errorQueue = err;
                        next();
                    }
                });
            });
            q.push(function (next) {
                if (models && models.length > 0) {
                    let result = model.find({ id: { $ne: models[0].id } }).limit(10).sort({ "price": -1 })
                        .exec(function (error, list) {
                        relationHelper_1.RelationHelper.loadRelations(list, req.params.category)
                            .then(function (result) {
                            otherItems = result;
                            next();
                        }, function (error) {
                            console.log('error', error);
                            errorQueue = error;
                            next();
                        });
                    });
                }
                else
                    next();
            });
            q.push((next) => {
                Marks_1.default.find({}).sort("name").exec((err, data) => {
                    if (err) {
                        next(err);
                    }
                    else {
                        listBrands = data;
                        next();
                    }
                });
            });
            q.start(function (error) {
                if (!errorQueue) {
                    console.log('models', models, models.id, models.price);
                    res.render('product-detail', { item: models, otherItems: otherItems, listBrands: listBrands, RelationHelper: relationHelper_1.RelationHelper });
                }
                else {
                    res.redirect("/404.html");
                }
            });
        }
        else
            res.send('no');
    }
    else
        res.redirect("/404.html");
});
module.exports = router;
//# sourceMappingURL=products.js.map