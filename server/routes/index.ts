let express = require('express'),
    router = express.Router(),
    queue = require('queue');

import brand from '../models/Marks';
import fridges from '../models/Productsfridges';
import {RelationHelper} from '../libs/relationHelper';

import conditioners from '../models/Productsconditioners';
import washings from '../models/Productswashings';
import hobs from '../models/Productshobs';
import ovens from '../models/Productsovens';

router.get('/', function (req, res, next) {

    let q = queue(),
        listProducts = [],
        listBrands = [];

    // Холодильники
    q.push( (next) => {
        fridges.find().limit(4)
            .then(function (list) {
                RelationHelper.loadRelations(list, 'fridges')
                    .then(function (models) {
                        listProducts = listProducts.concat(models);
                        next();
                    }, function (error) {
                        console.log('error', error);
                        next(error);
                    });

            }, function (error) {
                console.log('ERROR index load items', error);
                next(error);
            })
    });

    // Кондиционеры
    q.push( (next) => {
        conditioners.find().limit(4)
            .then(function (list) {
                RelationHelper.loadRelations(list, 'conditioners')
                    .then(function (models) {
                        listProducts = listProducts.concat(models);
                        next();
                    }, function (error) {
                        console.log('error', error);
                        next(error);
                    });
            }, function (error) {
                console.log('ERROR index load items', error);
                next(error);
            })
    });

    // Стир машинки
    q.push( (next) => {
        washings.find().limit(5)
            .then(function (list) {
                RelationHelper.loadRelations(list, 'washings')
                    .then(function (models) {
                        listProducts = listProducts.concat(models);
                        next();
                    }, function (error) {
                        console.log('error', error);
                        next(error);
                    });
            }, function (error) {
                console.log('ERROR index load items', error);
                next(error);
            })
    });

    // Варочные панели
    q.push( (next) => {
        hobs.find().limit(4)
            .then(function (list) {
                RelationHelper.loadRelations(list, 'hobs')
                    .then(function (models) {
                        listProducts = listProducts.concat(models);
                        next();
                    }, function (error) {
                        console.log('error', error);
                        next(error);
                    });
            }, function (error) {
                console.log('ERROR index load items', error);
                next(error);
            })
    });

    // Духовой шкаф
    q.push(function (next) {
        ovens.find().limit(4)
            .then(function (list) {
                RelationHelper.loadRelations(list, 'ovens')
                    .then(function (models) {
                        listProducts = listProducts.concat(models);
                        next();
                    }, function (error) {
                        console.log('error', error);
                        next();
                    });
            }, function (error) {
                console.log('ERROR index load items', error);
                next(error);
            })
    });

    // Собираем список брендов
    q.push(function (next) {
        brand.find({}).sort('name').exec(function (err, data) {
            if (err) {
                next(err);
            }
            else {
                listBrands = data;
                next();
            }
        });
    });

    q.start(function (error) {

        if (error) console.log('ERROR index load items', error);
        else {
            res.render('index', {listProducts: listProducts, listBrands: listBrands});
        }
    })

});

module.exports = router;