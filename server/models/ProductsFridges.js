"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.default = mongoose.model("ProductsFridges", new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    gallery: [],
    model: {
        type: String,
        required: true,
    },
    slug: {
        type: String,
        unique: true,
        required: true
    },
    category: {
        type: String,
    },
    image: {
        type: String,
    },
    image_dir: {
        type: String,
    },
    mark: {
        type: String,
        required: true,
    },
    color: {
        type: String,
    },
    volume: {
        type: String
    },
    manufacturer: {
        type: String,
    },
    price: {
        type: Number,
    },
    scope: {
        type: String
    },
    control: {
        type: String
    },
    energy_consumption: {
        type: String
    },
    energy_class: {
        type: String
    },
    location_freezer: {
        type: String
    },
    inverter: {
        type: String
    },
    number_cameras: {
        type: String
    },
    number_doors: {
        type: String
    },
    zone_freshness: {
        type: String
    },
    type_cooling: {
        type: String
    },
    dimensions: {
        type: String
    },
    climatic_class: {
        type: String
    },
    overweighing_door: {
        type: String
    },
    weight: {
        type: String
    },
    holidays_mode: {
        type: String
    },
    options: {
        type: String
    },
    display: {
        type: String
    },
    freezing_capacity: {
        type: String
    },
    refrigerant: {
        type: String
    },
    noise_level: {
        type: String
    },
    additional_features: {
        type: String
    },
}));
//# sourceMappingURL=Productsfridges.js.map