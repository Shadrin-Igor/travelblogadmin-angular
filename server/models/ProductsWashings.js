"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.default = mongoose.model("Productswashings", new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    gallery: [],
    image: {
        type: String,
    },
    slug: {
        type: String,
        required: true
    },
    category: {
        type: String,
    },
    image_dir: {
        type: String,
    },
    model: {
        type: String,
        required: true
    },
    mark: {
        type: String,
    },
    manufacturer: {
        type: String,
        required: true
    },
    price: {
        type: Number
    },
    download_type: {
        type: String
    },
    maximum_load: {
        type: String
    },
    drying: {
        type: String
    },
    control: {
        type: String
    },
    display: {
        type: String
    },
    direct_drive: {
        type: String
    },
    dimensions: {
        type: String
    },
    weight: {
        type: String
    },
    color: {
        type: String
    },
    energy_class: {
        type: String
    },
    number_revolutions: {
        type: String
    },
    protection_children: {
        type: String
    },
    control_imbalance: {
        type: String
    },
    foam_control: {
        type: String
    },
    number_programs: {
        type: String
    },
    programs: {
        type: String
    },
    delay_timer: {
        type: String
    },
    material: {
        type: String
    },
    loading_door: {
        type: String
    },
    noise: {
        type: String
    },
    loading_linen: {
        type: String
    },
    additional_features: {
        type: String
    },
    annual_consumption: {
        type: String
    },
    maximum_temperature: {
        type: String
    },
    door: {
        type: String
    }
}));
//# sourceMappingURL=Productswashings.js.map