"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.default = mongoose.model('collors', new mongoose.Schema({
    id: { type: String, required: true, unique: true },
    name: { type: String, required: true }
}));
//# sourceMappingURL=Collors.js.map