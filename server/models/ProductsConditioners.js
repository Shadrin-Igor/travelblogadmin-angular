"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.default = mongoose.model("Productsconditioners", new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    gallery: [],
    mark: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    category: {
        type: String,
    },
    image: {
        type: String,
    },
    image_dir: {
        type: String,
    },
    price: {
        type: Number,
    },
    color: {
        type: String,
    },
    manufacturer: {
        type: String,
        required: true
    },
    type: {
        type: String,
    },
    serviced_area: {
        type: String
    },
    energy_class: {
        type: String
    },
    modes: {
        type: String
    },
    cooling_capacity: {
        type: String
    },
    heating_power: {
        type: String
    },
    power_heating: {
        type: String
    },
    power_cooling: {
        type: String
    },
    additional_modes: {
        type: String
    },
    drying_mode: {
        type: String
    },
    d_u: {
        type: String
    },
    timer: {
        type: String
    },
    noise_level: {
        type: String
    },
    phase: {
        type: String
    },
    air_filters: {
        type: String
    },
    fan_control: {
        type: String
    },
    minimum_temperature: {
        type: String
    },
    additional_information: {
        type: String
    },
    other_functions: {
        type: String
    },
    dimensions: {
        type: String
    },
    weight_indoor: {
        type: String
    },
    dimensions_outdoor: {
        type: String
    },
    weight_external: {
        type: String
    },
    freon: {
        type: String
    }
}));
//# sourceMappingURL=Productsconditioners.js.map