"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.default = mongoose.model('marks', new mongoose.Schema({
    id: { type: String, required: true, unique: true },
    name: { type: String, required: true }
}));
//# sourceMappingURL=Marks.js.map