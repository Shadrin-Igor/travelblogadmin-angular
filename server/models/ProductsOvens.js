"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.default = mongoose.model("Productsovens", new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    gallery: [],
    mark: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    category: {
        type: String,
    },
    image: {
        type: String,
    },
    image_dir: {
        type: String,
    },
    manufacturer: {
        type: String,
        required: true
    },
    price: {
        type: Number,
    },
    type: {
        type: String,
    },
    controls: {
        type: String
    },
    scope: {
        type: String
    },
    dimensions: {
        type: String
    },
    modes_operation: {
        type: String
    },
    grill: {
        type: String
    },
    convection: {
        type: String
    },
    niche_size: {
        type: String
    },
    rotisserie: {
        type: String
    },
    oven_door: {
        type: String
    },
    lighting: {
        type: String
    },
    equipment: {
        type: String
    },
    energy_class: {
        type: String
    },
    maximum_temperature: {
        type: String
    },
    sleep_timer: {
        type: String
    },
    indication: {
        type: String
    },
    color: {
        type: String
    },
    display_type: {
        type: String
    }
}));
//# sourceMappingURL=Productsovens.js.map