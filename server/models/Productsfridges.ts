import * as mongoose from "mongoose";

export default mongoose.model("ProductsFridges",
    new mongoose.Schema({
        id: {
            type: String,
            required: true
        },
        gallery: [],
        model: {
            type: String,
            required: true,
        },
        slug: {
            type: String,
            unique: true,
            required: true
        },
        category: {
            type: String,
        },
        image: {
            type: String,
        },
        image_dir: {
            type: String,
        },
        mark: {
            type: String,
            required: true,
        },
        color: {
            type: String,
        },
        // Объем
        volume: {
            type: String
        },
        manufacturer: {
            type: String,
        },
        price: {
            type: Number,
        },
        scope: {
            type: String
        },
        // Управление
        control: {
            type: String
        },
        // Энергопотребление
        energy_consumption: {
            type: String
        },
        // Класс энергоэффективности
        energy_class: {
            type: String
        },
        // Расположение морозильной камеры
        location_freezer: {
            type: String
        },
        // Инверторный тип компрессора
        inverter: {
            type: String
        },
        // Количество камер
        number_cameras: {
            type: String
        },
        // Количество дверей
        number_doors: {
            type: String
        },
        //  Зона свежести
        zone_freshness: {
            type: String
        },
        // Тип охлаждения
        type_cooling: {
            type: String
        },
        // Габариты
        dimensions: {
            type: String
        },
        // Климатический класс
        climatic_class: {
            type: String
        },
        // Перевешивание двери
        overweighing_door: {
            type: String
        },
        // Вес
        weight: {
            type: String
        },
        // Режим «Отпуск»
        holidays_mode: {
            type: String
        },
        // Доп. Опции
        options: {
            type: String
        },
        // Дисплей
        display: {
            type: String
        },
        // Мощность замораживания
        freezing_capacity: {
            type: String
        },
        // Хладагент
        refrigerant: {
            type: String
        },
        // Уровень шума, дБ
        noise_level: {
            type: String
        },

        // Дополнительные возможности
        additional_features: {
            type: String
        },

    })
);