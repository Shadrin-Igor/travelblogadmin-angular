"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.default = mongoose.model("Productshobs", new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    gallery: [],
    mark: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    category: {
        type: String,
    },
    image: {
        type: String,
    },
    image_dir: {
        type: String,
    },
    manufacturer: {
        type: String,
        required: true
    },
    price: {
        type: Number,
    },
    type: {
        type: String,
    },
    dimensions: {
        type: String
    },
    dimensions_embedding: {
        type: String
    },
    material: {
        type: String
    },
    total_hotplates: {
        type: String
    },
    gas_burners: {
        type: String
    },
    type_consolidation: {
        type: String
    },
    location_control: {
        type: String
    },
    security_system: {
        type: String
    },
    electric_ignition: {
        type: String
    },
    gas_control: {
        type: String
    },
    iron_grilles: {
        type: String
    },
    color: {
        type: String
    },
    display_type: {
        type: String
    }
}));
//# sourceMappingURL=Productshobs.js.map