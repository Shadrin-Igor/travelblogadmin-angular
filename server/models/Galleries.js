"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Galleries = new mongoose.Schema({
    dir: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    item: {
        type: String,
        required: true
    },
});
exports.default = mongoose.model('Galleries', Galleries);
//# sourceMappingURL=Galleries.js.map