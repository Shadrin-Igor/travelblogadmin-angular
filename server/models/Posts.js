var mongoose = require('mongoose');

module.exports = mongoose.model('posts',
    new mongoose.Schema({
        id: { type: Number, required: true, unique: true },
        name: { type: String, required: true },
        description: { type: String, required: true },
        date: {type: Date, default: Date.now},
        category: { type: String}
    })
);