import * as mongoose from 'mongoose';

export default mongoose.model('marks',
    new mongoose.Schema({
        id: { type: String, required: true, unique: true },
        name: { type: String, required: true }
    })
);