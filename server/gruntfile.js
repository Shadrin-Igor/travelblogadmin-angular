module.exports = function(grunt) {
    "use strict";

    grunt.initConfig({
        ts: {
            app: {
                files: [{
                    src: ["\*\*/\*.ts", "!node_modules/\*\*/\*.ts", "!_all.d.ts"],
                    dest: "."
                }],
                options: {
                    emitDecoratorMetadata: true,
                    experimentalDecorators: true,
                    moduleResolution: "node",
                    removeComments: false,
                    module: "commonjs",
                    target: "es6",
                    sourceMap: false,
                    lib: ["ES2015.Promise"]
                },
                exclude: [
                    "node_modules"
                ]
            }
        },
        tslint: {
            options: {
                configuration: "tslint.json"
            },
            files: {
                src: ["\*\*/\*.ts"]
            }
        },
        watch: {
            ts: {
                files: ["\*\*/\*.ts", "!node_modules/\*\*/\*.ts"],
                tasks: ["ts", "tslint"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks("grunt-tslint");

    grunt.registerTask("default", [
        "ts",
        "tslint"
    ]);

};